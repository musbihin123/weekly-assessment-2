const fizzBuzz = (angka) => {
  for (let i = 0; i < angka; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
      console.log("fizzbuzz");
    } else if (i % 3 == 0) {
      console.log("fizz");
    } else if (i % 5 == 0) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
};

// input test
const input1 = 16;
const input2 = 100;

fizzBuzz(input1);
fizzBuzz(input2);
