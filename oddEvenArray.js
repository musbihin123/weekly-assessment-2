// write function here
const oddEvenArray = (angka) => {
  for (let i = 0; i < angka.length; i++) {
    if (angka[i] % 2 == 0) {
      console.log(`${angka[i]} genap`);
    } else {
      console.log(`${angka[i]} ganjil`);
    }
  }
};

// input test
const input1 = [1, 4, 6, 3, 10, 7];

oddEvenArray(input1);
