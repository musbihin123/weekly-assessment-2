const printNumberDescending = (input) => {
  for (let i = input; i > 0; i--) {
    console.log(i);
  }
  return;
};

// input test
const input1 = 5;

printNumberDescending(input1); // output: 0 1 2 3 4 5

// TASK 3
