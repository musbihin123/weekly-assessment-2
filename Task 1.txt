1.HTML (Hypertext Markup Language) works to create website frameworks such as headings, paragraphs, images, etc. HTML does not have a programming language, 
because it does not have the ability to process data according to the logic we want (Branching, Looping, etc.). Javascript is a scripting and programming 
language that functions to make websites more dynamic. Javascript works client-side, in terms of dynamics it is usually used to change light mode to dark mode, 
display prompts and alerts, manipulate html frameworks, validate data, and can perform arithmetic operations.

2. a). Let and Const adhere to a block scope system, in which the variable scope can only be accessed within the block.

   b). Var adheres to a functional scope system, where the variables can be accessed from inside or outside the block except outside the function.

   c). Data on Let and Var can be changed.

3. Null, Undefined, Boolean,Number, String, Symbol, NaN, Object, Array

4. Functions are sub-programs that can be reused either within the program itself or in other programs.A function in Javascript is an object. Because it has 
   properties and methods.

   There are 4 ways we can do to create a function in Javascript:
   a.Using the usual way;
   b.Using expressions;
   c.Using arrows (=>);
   d.using Constructor.
5. Hoisting is a concept in JavaScript, not a feature. In other scripting or server-side languages, variables or functions must be declared before using them.
   In JavaScript, variable and function names can be used before declaring them. The JavaScript compiler moves all variable and function declarations to the top
   so there won't be any errors.


   
